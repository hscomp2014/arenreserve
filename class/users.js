import React from 'react';
import male from '../img/male.png';
import female from '../img/female.png';
import conf from './conf';
const baseUrl = conf.baseUrl;
class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          "userid": -1,
          "userList":false,
          "userData":'در حال بارگزاری ...',
        };
      }
    getAllUsers=()=>{
        fetch(
            baseUrl+"therapist/list/",
            {
              method:"POST",
              headers: new Headers({
                'content-type': 'application/json'
              }),
            }
            ).then(res =>{
              this.setState({"userList":true});
              console.log(res);
              if(res.status!==200){
                this.setState({"userData":'خطا در ارتباط با سرور'});
                return false;
              }
              res.json().then(rep=>{
                console.log(rep);
                if(rep.status===1)
                    this.setState({"userData":this.userView(rep.data)});
                else
                    this.setState({"userData":rep.message});
              });
        });
    }
    userView = (inp)=>{
        let out=[];
        let tmp='';
        inp.map((value) => {
            value.lname = value.lname.split('-')[0];
            tmp=
                <div className="row no-gutters col-md-6 col-sm-12 col-xs-12 border rounded border-secondary bg-light" >
                    <div className="col-sm-3" >
                        <img style={{width:'100%'}} src={value.gender==='1'?female:male} alt="" ></img>
                    </div>
                    <div className="col-sm-5 mt-4" >
                        <div className="font-weight-bold text-dark hs-big" >
                        {value.fname+' '+value.lname}
                        </div>
                        <div className="text-dark small">
                        تخصص: روان درمانگر تخیلی
                        </div>
                    </div>
                    <div className="col-sm-4 text-center mt-4" >
                        <button className="btn btn-success btn-lg" onClick={()=>{this.setState({userid:value.user_id})} }>
                            رزرو نوبت
                        </button>
                    </div>
                </div>
                ;
            out.push(tmp);
        });
        out=<div className="row no-gutters mt-3 align-items-center">
            {out}
        </div>;
        return out;
    }
    showUserTimes = (userid)=>{
        let out = <h1>زمانهای کاربر {userid}</h1> ;
        return out;
    }
    componentDidMount(){
        if(this.state.userList===false){
            this.getAllUsers();
            this.setState({userList:true});
        }
    }
    render() {
        let out='';
        if(this.state.userid===-1){
            out = this.state.userData;
        }
        else{
            out = this.showUserTimes(this.state.userid);
        }
        return out;
    }
}
export default Users;
